package controllers

import (
	"strings"

	"employee_test/database"
	"employee_test/models"

	"github.com/gofiber/fiber/v2"
)

// เพิ่มลูกจ้าง
func AddEmployee(c *fiber.Ctx) error {
	db := database.DBConn
	var employee models.Employees

	if err := c.BodyParser(&employee); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Create(&employee)
	return c.Status(201).JSON(employee)
}

/// แสดงลูกจ้างทั้งหมด
func GetEmployees(c *fiber.Ctx) error {
	db := database.DBConn
	var emlpoyees []models.Employees

	db.Find(&emlpoyees)
	return c.Status(200).JSON(emlpoyees)
}

/// หาตามไอดี ?search= (employee_id)
func GetEmployee(c *fiber.Ctx) error {
	db := database.DBConn
	search := strings.TrimSpace(c.Query("search"))
	var employee []models.Employees

	result := db.Find(&employee, "employee_id = ?", search)

	// returns found records count, equals `len(users)
	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}
	return c.Status(200).JSON(&employee)
}

// แก้ไขข้อมูลลูกจ้าง
func UpdateEmployee(c *fiber.Ctx) error {
	db := database.DBConn
	var employee models.Employees
	id := c.Params("id")

	if err := c.BodyParser(&employee); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Where("id = ?", id).Updates(&employee)
	return c.Status(200).JSON(employee)
}

/// ลบลูกจ้าง
func RemoveEmployee(c *fiber.Ctx) error {
	db := database.DBConn
	id := c.Params("id")
	var employee models.Employees

	result := db.Delete(&employee, id)

	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}

	return c.SendStatus(200)
}
