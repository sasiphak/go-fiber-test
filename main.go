package main

import (
	"employee_test/database"
	"employee_test/models"
	"employee_test/routes"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func initDatabase() {
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=true&loc=Local",
		"root",
		"",
		"127.0.0.1",
		"3306",
		"employee_test",
	)
	var err error
	database.DBConn, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	fmt.Println("Database connected!")
	// log.Println(db)
	database.DBConn.AutoMigrate(&models.Employees{})
	fmt.Println("Migrated DB")
}

func main() {
	app := fiber.New()
	initDatabase()        ///db
	routes.UserRoute(app) ///สร้างไว้เพื่อนำไปเรียกใช้กับไฟล์อื่นได้

	app.Listen(":3001")
}
