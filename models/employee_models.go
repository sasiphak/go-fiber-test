package models

type Employees struct {
	Employee_id int    `json:"employee_id"`
	Name        string `json:"name"`
	LastName    string `json:"lastname"`
	Birthday    string `json:"birthday"`
	Age         int    `json:"age"`
	Email       string `json:"email"`
	Tel         string `json:"tel"`
}
