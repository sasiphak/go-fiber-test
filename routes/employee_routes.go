package routes

import (
	"employee_test/controllers"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/basicauth"
)

func UserRoute(app *fiber.App) {

	app.Use(basicauth.New(basicauth.Config{
		Users: map[string]string{
			"testgo": "772565",
		},
	}))

	api := app.Group("/api") // /api
	v1 := api.Group("/v1")   // /api/v1

	v1.Post("/emp", controllers.AddEmployee)          ///เพิ่ม
	v1.Get("/emp", controllers.GetEmployees)          ///เแสดงหมด
	v1.Put("/emp/:id", controllers.UpdateEmployee)    ///แก้ไขข้อมูล
	v1.Delete("/emp/:id", controllers.RemoveEmployee) ///ลบ
	v1.Get("/emp/id", controllers.GetEmployee)        //////http://localhost:3000/api/v1/employee/id?search=1
}
